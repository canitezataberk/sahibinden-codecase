package com.sahibinden.backend.repo.query;

import com.sahibinden.common.dto.ad.MatchCriteria;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Objects;

public class AdQuery {

    public static Query filter(MatchCriteria matchCriteria){
        Query query = new Query();
        if (Objects.nonNull(matchCriteria.getCategory())){
            query.addCriteria(Criteria.where("categoryList").all(matchCriteria.getCategory()));
        }
        if (Objects.nonNull(matchCriteria.getClientType())){
            query.addCriteria(Criteria.where("clientType").is(matchCriteria.getClientType()));
        }
        if (Objects.nonNull(matchCriteria.getLocation())){
            query.addCriteria(Criteria.where("locations").all(matchCriteria.getLocation()));
        }

        return query;
    }
}
