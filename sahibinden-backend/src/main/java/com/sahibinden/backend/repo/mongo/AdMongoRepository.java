package com.sahibinden.backend.repo.mongo;

import com.sahibinden.backend.model.AdDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AdMongoRepository extends MongoRepository<AdDocument, String> {

}
