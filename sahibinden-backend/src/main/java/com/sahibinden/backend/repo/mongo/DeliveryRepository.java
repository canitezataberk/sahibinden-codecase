package com.sahibinden.backend.repo.mongo;

import com.sahibinden.backend.model.DeliveryDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DeliveryRepository extends MongoRepository<DeliveryDocument, String> {
}
