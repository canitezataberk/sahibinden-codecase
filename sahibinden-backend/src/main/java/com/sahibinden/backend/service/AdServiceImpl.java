package com.sahibinden.backend.service;

import com.sahibinden.backend.mapper.AdMapper;
import com.sahibinden.backend.mapper.AdMapperImpl;
import com.sahibinden.backend.mapper.DeliveryConverter;
import com.sahibinden.backend.model.AdDocument;
import com.sahibinden.backend.model.DeliveryDocument;
import com.sahibinden.backend.repo.query.AdQuery;
import com.sahibinden.backend.service.persistence.AdPersistenceService;
import com.sahibinden.common.dto.ad.AdCreateRequest;
import com.sahibinden.common.dto.ad.AdResponse;
import com.sahibinden.common.dto.ad.AdStatistic;
import com.sahibinden.common.dto.ad.DeliveryResult;
import com.sahibinden.common.dto.ad.MatchCriteria;
import com.sahibinden.common.service.AdService;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.sahibinden.backend.advice.constants.DeliveryFlags.VISITED;
import static com.sahibinden.backend.utils.AdServiceUtil.isCreateRequestValid;

@Service
public class AdServiceImpl implements AdService {

    private final AdPersistenceService adPersistenceService;
    private final AdMapper adMapper;
    private final MongoTemplate mongoTemplate;
    private final DeliveryConverter deliveryConverter;
    private final DeliveryService deliveryService;

    public AdServiceImpl(AdPersistenceService adPersistenceService,
                         AdMapper adMapper, MongoTemplate mongoTemplate,
                         DeliveryConverter deliveryConverter, DeliveryService deliveryService) {
        this.adPersistenceService = adPersistenceService;
        this.adMapper = adMapper;
        this.mongoTemplate = mongoTemplate;
        this.deliveryConverter = deliveryConverter;
        this.deliveryService = deliveryService;
    }

    @Override
    public AdResponse createAd(AdCreateRequest adCreateRequest) {
        if (!isCreateRequestValid(adCreateRequest))
            return null;

        AdDocument adDocument = adMapper.toAdDocument(adCreateRequest);
        adPersistenceService.save(adDocument);
        addMissingFieldsToAdDocument(adDocument);
        return adMapper.toAdResponse(adDocument);
    }

    @Override
    public AdStatistic getAdStatistic(String adId) {
        return adPersistenceService.findById(adId).getAdStatistic();
    }

    @Override
    public DeliveryResult getWinner(MatchCriteria matchCriteria) {
        AdDocument adDocument = getAdDocumentByCriteria(matchCriteria);
        if (Objects.isNull(adDocument) || deliveryService.isDeliveryReturnable(adDocument, matchCriteria))
            return new DeliveryResult();

        incrementUserImpressionForVisitor(adDocument, matchCriteria);
        DeliveryDocument deliveryDocument = deliveryService.createDelivery(adMapper.toAdResponse(adDocument));
        return deliveryConverter.toDeliveryResult(deliveryDocument);
    }

    @Override
    public void processImpression(String deliveryId) {
        DeliveryDocument deliveryDocument = deliveryService.findById(deliveryId);
        if (Objects.isNull(deliveryDocument) || deliveryDocument.isVisited()) return;

        deliveryService.setVisited(deliveryDocument, VISITED);

        AdDocument adDocument = adPersistenceService.findById(deliveryDocument.getAdResponse().getId());
        adDocument.getAdStatistic().impression();
        adPersistenceService.save(adDocument);
    }

    @Override
    public void processClick(String deliveryId) {
        DeliveryDocument deliveryDocument = deliveryService.findById(deliveryId);
        if (Objects.isNull(deliveryDocument) || deliveryDocument.isVisited()) return;

        deliveryService.setVisited(deliveryDocument, VISITED);

        AdDocument adDocument = adPersistenceService.findById(deliveryDocument.getAdResponse().getId());

        if (!adDocument.isClickable()) return;
        adDocument.getAdStatistic().click();
        adPersistenceService.save(adDocument);
    }

    @Override
    public void deleteAll() {
        adPersistenceService.deleteAll();
    }

    private AdDocument getAdDocumentByCriteria(MatchCriteria matchCriteria){
        Query adQuery = AdQuery.filter(matchCriteria);
        return mongoTemplate.findOne(adQuery, AdDocument.class);
    }

    private void incrementUserImpressionForVisitor(AdDocument adDocument, MatchCriteria matchCriteria){
        adDocument.incrementUserImpressionForUser(matchCriteria);
        adPersistenceService.save(adDocument);
    }

    private void addMissingFieldsToAdDocument(AdDocument adDocument) {
        AdStatistic adStatistic = new AdStatistic();
        adStatistic.setAdId(adDocument.getId());
        adStatistic.setClickCount(0L);
        adStatistic.setImpressionCount(0L);
        adDocument.setAdStatistic(adStatistic);

        Map<String, Integer> map = new HashMap<>();
        adDocument.setUserImpressions(map);

        adPersistenceService.save(adDocument);
    }
}
