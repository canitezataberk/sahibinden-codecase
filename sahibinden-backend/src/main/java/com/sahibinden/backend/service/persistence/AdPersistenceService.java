package com.sahibinden.backend.service.persistence;

import com.sahibinden.backend.advice.Exception.NoSuchAdException;
import com.sahibinden.backend.model.AdDocument;
import com.sahibinden.backend.repo.mongo.AdMongoRepository;
import org.springframework.stereotype.Service;

@Service
public class AdPersistenceService {

    private final AdMongoRepository adMongoRepository;

    public AdPersistenceService(AdMongoRepository adMongoRepository) {
        this.adMongoRepository = adMongoRepository;
    }

    public void save(AdDocument adDocument){
        adMongoRepository.save(adDocument);
    }

    public AdDocument findById(String id) {
        return adMongoRepository.findById(id).orElseThrow(NoSuchAdException::new);
    }

    public void deleteAll(){
        adMongoRepository.deleteAll();
    }
}
