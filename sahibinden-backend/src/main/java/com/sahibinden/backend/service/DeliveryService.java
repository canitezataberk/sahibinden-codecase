package com.sahibinden.backend.service;

import com.sahibinden.backend.mapper.DeliveryConverter;
import com.sahibinden.backend.model.AdDocument;
import com.sahibinden.backend.model.DeliveryDocument;
import com.sahibinden.backend.service.persistence.DeliveryPersistenceService;
import com.sahibinden.common.dto.ad.AdResponse;
import com.sahibinden.common.dto.ad.MatchCriteria;
import org.springframework.stereotype.Service;

@Service
public class DeliveryService {

    private final DeliveryPersistenceService deliveryPersistenceService;
    private final DeliveryConverter deliveryConverter;

    public DeliveryService(DeliveryPersistenceService deliveryPersistenceService, DeliveryConverter deliveryConverter) {
        this.deliveryPersistenceService = deliveryPersistenceService;
        this.deliveryConverter = deliveryConverter;
    }

    public DeliveryDocument createDelivery(AdResponse adResponse){
        DeliveryDocument deliveryDocument = deliveryConverter.toDeliveryDocument(adResponse);
        deliveryPersistenceService.save(deliveryDocument);
        return deliveryDocument;
    }

    public DeliveryDocument findById(String id){
        return deliveryPersistenceService.findById(id);
    }

    public void setVisited(DeliveryDocument deliveryDocument, boolean visitStatus){
        deliveryDocument.setVisited(visitStatus);
        deliveryPersistenceService.save(deliveryDocument);
    }

    public boolean isDeliveryReturnable(AdDocument adDocument, MatchCriteria matchCriteria){
        boolean isClickable = adDocument.isClickable();
        int visitorImpressionCount = adDocument.getImpressionCountForVisitor(matchCriteria);

        boolean isUserImpressionExceedFrequencyCapping = visitorImpressionCount >= adDocument.getFrequencyCapping();

        return !isClickable || isUserImpressionExceedFrequencyCapping;
    }

}
