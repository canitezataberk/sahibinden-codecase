package com.sahibinden.backend.service.persistence;

import com.sahibinden.backend.model.DeliveryDocument;
import com.sahibinden.backend.repo.mongo.DeliveryRepository;
import org.springframework.stereotype.Service;

@Service
public class DeliveryPersistenceService {
    private final DeliveryRepository deliveryRepository;

    public DeliveryPersistenceService(DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    public void save(DeliveryDocument deliveryDocument){
        deliveryRepository.save(deliveryDocument);
    }

    public DeliveryDocument findById(String id){
        return deliveryRepository.findById(id).orElse(null);
    }

}
