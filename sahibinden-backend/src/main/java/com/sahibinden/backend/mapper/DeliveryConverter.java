package com.sahibinden.backend.mapper;

import com.sahibinden.backend.model.DeliveryDocument;
import com.sahibinden.common.dto.ad.AdResponse;
import com.sahibinden.common.dto.ad.DeliveryResult;
import org.springframework.stereotype.Component;

import static com.sahibinden.backend.advice.constants.DeliveryFlags.NOT_VISITED;

@Component
public class DeliveryConverter {

    public DeliveryResult toDeliveryResult(DeliveryDocument deliveryDocument){
        DeliveryResult deliveryResult = new DeliveryResult();
        deliveryResult.setAdResponse(deliveryDocument.getAdResponse());
        deliveryResult.setDeliveryId(deliveryDocument.getDeliveryId());

        return deliveryResult;
    }

    public DeliveryDocument toDeliveryDocument(AdResponse adResponse){
        DeliveryDocument deliveryDocument = new DeliveryDocument();
        deliveryDocument.setVisited(NOT_VISITED);
        deliveryDocument.setAdResponse(adResponse);

        return deliveryDocument;
    }

}
