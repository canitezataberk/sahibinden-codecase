package com.sahibinden.backend.utils;

import com.sahibinden.backend.validator.AdCreateRequestValidator;
import com.sahibinden.common.dto.ad.AdCreateRequest;
import org.springframework.stereotype.Component;

@Component
public class AdServiceUtil {

    public static boolean isCreateRequestValid(AdCreateRequest adCreateRequest){
        return AdCreateRequestValidator.validate(adCreateRequest);
    }

}
