package com.sahibinden.backend.advice.Exception;

import static com.sahibinden.backend.advice.constants.ErrorCodes.AD_NOT_FOUND;

public class NoSuchAdException extends RuntimeException{
    private final int code;
    public NoSuchAdException(){
        super("No such Ad");
        this.code = AD_NOT_FOUND;
    }

    public int getCode() {
        return code;
    }
}
