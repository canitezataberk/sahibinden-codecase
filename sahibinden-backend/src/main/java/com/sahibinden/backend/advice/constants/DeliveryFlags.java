package com.sahibinden.backend.advice.constants;

public class DeliveryFlags {
    public static final boolean VISITED = Boolean.TRUE;
    public static final boolean NOT_VISITED = Boolean.FALSE;
}
