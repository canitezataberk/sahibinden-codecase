package com.sahibinden.backend.advice.constants;

public class RequestConstants {
    public static final int MINIMUM_TITLE_LENGTH = 10;
    public static final int MAXIMUM_TITLE_LENGTH = 30;
    public static final int MINIMUM_DESCRIPTION_LENGTH = 30;
    public static final int MAXIMUM_DESCRIPTION_LENGTH = 100;
    public static final int MINIMUM_BID_PRICE = 50;
    public static final int MAXIMUM_BID_PRICE = 300;
    public static final int TOTAL_BUDGET_MULTIPLIER = 10;
    public static final int MINIMUM_FREQUENCY_CAPPING = 6;
    public static final int MAXIMUM_FREQUENCY_CAPPING = 24;
    public static final int MINIMUM_LOCATION_VALUE = 1;
    public static final int MAXIMUM_LOCATION_VALUE = 81;
    public static final int MINIMUM_CATEGORY_LIST_SIZE = 1;
}
