package com.sahibinden.backend.advice;

import com.sahibinden.backend.advice.Exception.NoSuchAdException;
import com.sahibinden.common.model.error.Error;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class SahibindenCodebaseControllerAdvice {

    @ExceptionHandler(NoSuchAdException.class)
    public ResponseEntity<Error> handleException(NoSuchAdException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(constructError(e.getCode(), e.getMessage()));
    }

    private Error constructError(int code, String message) {
        return new Error(code, message, new Date().getTime());
    }
}
