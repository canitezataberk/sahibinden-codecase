package com.sahibinden.backend.validator;

import com.sahibinden.common.dto.ad.AdCreateRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.*;

import static com.sahibinden.backend.advice.constants.RequestConstants.*;

public class AdCreateRequestValidator {

    public static boolean validate(AdCreateRequest request){

        if (!isBetween(MINIMUM_TITLE_LENGTH, MAXIMUM_TITLE_LENGTH, request.getTitle().length()))
            return false;
        if (!isBetween(MINIMUM_DESCRIPTION_LENGTH, MAXIMUM_DESCRIPTION_LENGTH, request.getDescription().length()))
            return false;
        if (!isBetween(MINIMUM_BID_PRICE, MAXIMUM_BID_PRICE, request.getBidPrice().intValue()))
            return false;
        if (!isValidTotalBudget(request.getTotalBudget(), request.getBidPrice()))
            return false;
        if (!isValidURL(request.getLink()))
            return false;
        if (containsBadWord(request.getTitle(), request.getDescription()))
            return false;
        if (request.getFrequencyCapping() < MINIMUM_FREQUENCY_CAPPING || request.getFrequencyCapping() > MAXIMUM_FREQUENCY_CAPPING)
            return false;
        if (!isLocationsValid(request.getLocations()))
            return false;
        if (Objects.isNull(request.getClientType()))
            return false;
        if (request.getCategoryList().size() < MINIMUM_CATEGORY_LIST_SIZE)
            return false;

        return true;
    }

    private static boolean isBetween(int min, int max, int actual){
        return min < actual && actual < max;
    }

    private static boolean isValidTotalBudget(Long totalBudget, Long bidPrice){
        return totalBudget >= bidPrice * TOTAL_BUDGET_MULTIPLIER;
    }

    private static boolean isValidURL(String urlString){
        try {
            URL url = new URL(urlString);
            url.toURI();
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    private static boolean containsBadWord(String title, String description) {
        try {
            Scanner scanner = new Scanner(new File("badWords.txt"));
            Set<String> badWordsSet = new HashSet<>();

            while (scanner.hasNextLine())
                badWordsSet.add(scanner.next());

            String[] titleWords = title.split("\\W+");
            String[] descWords = description.split("\\W+");

            List<String> allWords = new ArrayList<>();
            allWords.addAll(Arrays.asList(titleWords));
            allWords.addAll(Arrays.asList(descWords));

            for (String word : allWords) {
                if (badWordsSet.contains(word))
                    return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean isLocationsValid(List<Long> locations){
        for(Long location : locations)
            if (location < MINIMUM_LOCATION_VALUE || location > MAXIMUM_LOCATION_VALUE)
                return false;

        return true;
    }
}
