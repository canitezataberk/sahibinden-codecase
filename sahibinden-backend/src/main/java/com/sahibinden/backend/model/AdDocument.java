package com.sahibinden.backend.model;

import com.sahibinden.common.dto.ad.AdStatistic;
import com.sahibinden.common.dto.ad.Category;
import com.sahibinden.common.dto.ad.ClientType;
import com.sahibinden.common.dto.ad.MatchCriteria;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@Document(value = "ad_collection")
public class AdDocument {

   @Id
   private String id;

   private ClientType clientType;

   private List<Category> categoryList;

   private Long bidPrice;

   private Long totalBudget;

   private Long frequencyCapping;

   private List<Long> locations;

   private String title;

   private String description;

   private String link;

   private AdStatistic adStatistic;

   private Map<String, Integer> userImpressions;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public ClientType getClientType() {
      return clientType;
   }

   public void setClientType(ClientType clientType) {
      this.clientType = clientType;
   }

   public List<Category> getCategoryList() {
      return categoryList;
   }

   public void setCategoryList(List<Category> categoryList) {
      this.categoryList = categoryList;
   }

   public Long getBidPrice() {
      return bidPrice;
   }

   public void setBidPrice(Long bidPrice) {
      this.bidPrice = bidPrice;
   }

   public Long getTotalBudget() {
      return totalBudget;
   }

   public void setTotalBudget(Long totalBudget) {
      this.totalBudget = totalBudget;
   }

   public Long getFrequencyCapping() {
      return frequencyCapping;
   }

   public void setFrequencyCapping(Long frequencyCapping) {
      this.frequencyCapping = frequencyCapping;
   }

   public List<Long> getLocations() {
      return locations;
   }

   public void setLocations(List<Long> locations) {
      this.locations = locations;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getLink() {
      return link;
   }

   public void setLink(String link) {
      this.link = link;
   }

   public AdStatistic getAdStatistic(){
      return this.adStatistic;
   }

   public void setAdStatistic(AdStatistic adStatistic){
      this.adStatistic = adStatistic;
   }

   public Map<String, Integer> getUserImpressions() {
      return userImpressions;
   }

   public void setUserImpressions(Map<String, Integer> userImpressions) {
      this.userImpressions = userImpressions;
   }

   public boolean isClickable() {
      final long clicksNeededForBudget = (this.getTotalBudget() / this.getBidPrice()) + 1;
      AdStatistic adStatistic = this.getAdStatistic();
      return adStatistic.getClickCount() < clicksNeededForBudget;
   }

   public int getImpressionCountForVisitor(MatchCriteria matchCriteria){
      if(!this.getUserImpressions().containsKey(matchCriteria.getVisitorId())){
         return 0;
      }
      return this.getUserImpressions().get(matchCriteria.getVisitorId());
   }

   public void incrementUserImpressionForUser(MatchCriteria matchCriteria){
      String visitorId = matchCriteria.getVisitorId();
      Map<String, Integer> userImpressions = this.getUserImpressions();

      userImpressions.put(visitorId, userImpressions.getOrDefault(visitorId, 0) + 1);

      this.userImpressions = userImpressions;
   }

}
