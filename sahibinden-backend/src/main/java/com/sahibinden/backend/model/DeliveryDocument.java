package com.sahibinden.backend.model;

import com.sahibinden.common.dto.ad.AdResponse;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("delivery")
public class DeliveryDocument {
    @Id
    private String deliveryId;
    private AdResponse adResponse;
    private boolean visited;

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public AdResponse getAdResponse() {
        return adResponse;
    }

    public void setAdResponse(AdResponse adResponse) {
        this.adResponse = adResponse;
    }

    public String getDeliveryId() {
        return deliveryId;
    }
}
